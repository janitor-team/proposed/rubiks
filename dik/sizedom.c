#include <stdio.h>

#define SUBGROUP
#define EQUIV
#define NOMAX
#include "trans/inv.a"
#include "trans/perm8.a"
#ifdef CCPERM
#include "trans/cperm.a"
#include "trans/cperm.r"
#include "trans/cperm.c"
#include "trans/eperm.a"
#include "trans/eperm.r"
#include "trans/eperm.m"
#endif /* CCPERM */
#ifdef CEPERM
#include "trans/eperm.a"
#include "trans/eperm.r"
#include "trans/eperm.c"
#include "trans/cperm.a"
#include "trans/cperm.r"
#include "trans/cperm.m"
#endif /* CEPERM */

#ifdef CCPERM
#define MAX_TURNS    (MAX_EPERMS * CPERM_MAGIC)
#endif /* CCPERM */
#ifdef CEPERM
#define MAX_TURNS    (MAX_CPERMS * EPERM_MAGIC)
#endif /* CEPERM */

#include "longtype.h"

TYPE turns1[MT];
TYPE turns2[MT];
long left = MAX_TURNS;
int quarter;
int maximal;
int gcount;
int givemax;

do_pr(turns, count)
int turns, count;
{
    if(maximal != 0 && turns != 1) {
	printf(", %10d maximal", maximal);
    }
    printf("\n");
    gcount = count;
    printf("%10d with %2d turns", count, turns);
    fflush(stdout);
}

try_one(num, count)
int num, count;
{
#ifdef CCPERM
int cperm, ncperm, ncperm1, cperm0;
int eperm, neperm, neperm1;
#endif /* CCPERM */
#ifdef CEPERM
int eperm, neperm, neperm1, eperm0;
int cperm, ncperm, ncperm1;
#endif /* CEPERM */
int i, j, nnum, counted = 0, k, l, max, wcounted = 0, m;

#ifdef CCPERM
    cperm0 = num / MAX_EPERMS;
    eperm = num - cperm0 * MAX_EPERMS;
    max = cperms_weights[cperm0];
    cperm = cperms_presents[cperm0];
#endif /* CCPERM */
#ifdef CEPERM
    eperm0 = num / MAX_CPERMS;
    cperm = num - eperm0 * MAX_CPERMS;
    max = eperms_weights[eperm0];
    eperm = eperms_presents[eperm0];
#endif /* CEPERM */
    for(i = 0; i < 6; i++) {
	ncperm = cperm;
	neperm = eperm;
	for(j = 0; j < 3; j++) {
	    ncperm = cperms[ncperm][i];
	    neperm = eperms[neperm][i];
	    if(quarter && j == 1) {
		continue;
	    }
#ifdef CCPERM
	    ncperm1 = cperms_pointer[ncperm];
	    for(m = 0; m < MI; m++) {
		if(cperms_rotate[ncperm][m]) {
		    neperm1 = eperms_trans[neperm][m];
		    nnum = ncperm1 * MAX_EPERMS + neperm1;
#endif /* CCPERM */
#ifdef CEPERM
	    neperm1 = eperms_pointer[neperm];
	    for(m = 0; m < MI; m++) {
		if(eperms_rotate[neperm][m]) {
		    ncperm1 = cperms_trans[ncperm][m];
		    nnum = neperm1 * MAX_CPERMS + ncperm1;
#endif /* CCPERM */
		    k = (nnum >> POW);
		    l = (1 << (nnum & MASK));
		    if((turns1[k] & l) == 0) {
			if((turns2[k] & l) == 0) {
			    turns2[k] |= l;
			    counted++;
#ifdef CCPERM
			    wcounted += cperms_weights[ncperm1];
#endif /* CCPERM */
#ifdef CEPERM
			    wcounted += eperms_weights[neperm1];
#endif /* CEPERM */
			}
			max = 0;
		    }
		}
	    }
	    if(i != 2 && i != 5) {
		break;
	    }
	}
    }
#ifdef IPERM
    cperm = cperms_inv[cperm];
    eperm = eperms_inv[eperm];
    for(i = 0; i < 6; i++) {
	ncperm = cperm;
	neperm = eperm;
	for(j = 0; j < 3; j++) {
	    ncperm = cperms[ncperm][i];
	    neperm = eperms[neperm][i];
	    if(quarter && j == 1) {
		continue;
	    }
#ifdef CCPERM
	    ncperm1 = cperms_pointer[ncperm];
	    for(m = 0; m < MI; m++) {
		if(cperms_rotate[ncperm][m]) {
		    neperm1 = eperms_trans[neperm][m];
		    nnum = ncperm1 * MAX_EPERMS + neperm1;
#endif /* CCPERM */
#ifdef CEPERM
	    neperm1 = eperms_pointer[neperm];
	    for(m = 0; m < MI; m++) {
		if(eperms_rotate[neperm][m]) {
		    ncperm1 = cperms_trans[ncperm][m];
		    nnum = neperm1 * MAX_CPERMS + ncperm1;
#endif /* CCPERM */
		    k = (nnum >> POW);
		    l = (1 << (nnum & MASK));
		    if((turns1[k] & l) == 0) {
			if((turns2[k] & l) == 0) {
			    turns2[k] |= l;
			    counted++;
#ifdef CCPERM
			    wcounted += cperms_weights[ncperm1];
#endif /* CCPERM */
#ifdef CEPERM
			    wcounted += eperms_weights[neperm1];
#endif /* CEPERM */
			}
			max = 0;
		    }
		}
	    }
	    if(i != 2 && i != 5) {
		break;
	    }
	}
    }
#endif /* IPERM */
    left -= counted;
    maximal += max;
    return wcounted;
}

try(count)
int count;
{
int i, counted = 0, k;
TYPE j;

    maximal = 0;
    for(i = 0; i < MT; i++) {
	if(turns2[i] != 0) {
	    turns1[i] |= turns2[i];
	}
    }
    for(i = 0; i < MT; i++) {
	if((turns1[i] & turns2[i]) != 0) {
	    j = turns1[i] & turns2[i];
	    turns2[i] &= (j ^ (~(TYPE)0));
	    k = 0;
	    while(j != 0) {
		if(j & 1) {
		    counted += try_one((i << POW) + k, count + 1);
		}
		k++;
		j >>= 1;
	    }
	}
    }
    do_pr(count + 1, counted);
}

static void init_turn(cperm, eperm)
int cperm, eperm;
{
int num, k, l, cperm1, eperm1, m;

#ifdef CCPERM
    cperm1 = cperms_pointer[cperm];
    for(m = 0; m < MI; m++) {
	if(cperms_rotate[cperm][m]) {
	    eperm1 = eperms_trans[eperm][m];
	    num = cperm1 * MAX_EPERMS + eperm1;
#endif /* CCPERM */
#ifdef CEPERM
    eperm1 = eperms_pointer[eperm];
    for(m = 0; m < MI; m++) {
	if(eperms_rotate[eperm][m]) {
	    cperm1 = cperms_trans[cperm][m];
	    num = eperm1 * MAX_CPERMS + cperm1;
#endif /* CEPERM */
	    k = (num >> POW);
	    l = (1 << (num & MASK));
	    if((turns2[k] & l) == 0) {
		turns2[k] |= l;
#ifdef CCPERM
		left -= cperms_weights[cperm1];
#endif /* CCPERM */
#ifdef CEPERM
		left -= eperms_weights[eperm1];
#endif /* CEPERM */
	    }
	}
    }
}

static void init_turns(min)
int min;
{
int cperm, eperm;

    init_turn(0, 0);
    if(min == 1) {
	return;
    }
    cperm = perm8_val(3,0,1,2,7,4,5,6);
    eperm = perm8_val(3,0,1,2,7,4,5,6);
    init_turn(cperm, eperm);
    cperm = perm8_val(2,3,0,1,6,7,4,5);
    eperm = perm8_val(2,3,0,1,6,7,4,5);
    init_turn(cperm, eperm);
    cperm = perm8_val(1,2,3,0,5,6,7,4);
    eperm = perm8_val(1,2,3,0,5,6,7,4);
    init_turn(cperm, eperm);
    if(min == 4) {
	return;
    }
    cperm = perm8_val(0,3,2,1,4,7,6,5);
    eperm = perm8_val(3,2,1,0,7,6,5,4);
    init_turn(cperm, eperm);
    cperm = perm8_val(1,0,3,2,5,4,7,6);
    eperm = perm8_val(0,3,2,1,4,7,6,5);
    init_turn(cperm, eperm);
    cperm = perm8_val(2,1,0,3,6,5,4,7);
    eperm = perm8_val(1,0,3,2,5,4,7,6);
    init_turn(cperm, eperm);
    cperm = perm8_val(3,2,1,0,7,6,5,4);
    eperm = perm8_val(2,1,0,3,6,5,4,7);
    init_turn(cperm, eperm);
}

main(argc, argv)
int argc;
char *argv[];
{
int i, k, error = 0, mininit;
TYPE j;

    quarter = -1;
    mininit = -1;
    givemax = -1;
    if(argc > 4) {
	fprintf(stderr, "Too many parameters\n");
	exit(1);
    }
    while(argc > 1) {
	if(!strcmp(argv[1], "-q")) {
	    if(quarter >= 0) {
		error = 1;
	    }
	    quarter = 1;
	} else if(!strcmp(argv[1], "-1")) {
	    if(mininit >= 0) {
		error = 1;
	    }
	    mininit = 1;
	} else if(!strcmp(argv[1], "-4")) {
	    if(mininit >= 0) {
		error = 1;
	    }
	    mininit = 4;
	} else if(!strcmp(argv[1], "-8")) {
	    if(mininit >= 0) {
		error = 1;
	    }
	    mininit = 8;
	} else if(!strcmp(argv[1], "-m")) {
	    if(givemax >= 0) {
		error = 1;
	    }
	    givemax = 1;
	}
	argc--;
	argv++;
    }
    if(error) {
	fprintf(stderr, "usage: sizedom [-q] [-1|-4|-8] [-m]\n");
	exit(1);
    }
    if(quarter < 0) {
	quarter = 0;
    }
    if(mininit < 0) {
	mininit = 4;
    }
    if(givemax < 0) {
	givemax = 0;
    }
#ifdef CCPERM
#ifdef VERBOSE
    fprintf(stderr, "Initializing corner permutations...\n");
#endif /* VERBOSE */
    init_cperms();
#ifdef VERBOSE
    fprintf(stderr, "Done!\n");
    fprintf(stderr, "Collapsing corner permutations...\n");
#endif /* VERBOSE */
    init_cperms1();
#ifdef VERBOSE
    fprintf(stderr, "Done!\n");
    fprintf(stderr, "Initializing edge permutations...\n");
#endif /* VERBOSE */
    init_eperms();
#ifdef VERBOSE
    fprintf(stderr, "Done!\n");
    fprintf(stderr, "Matching edge permutations...\n");
#endif /* VERBOSE */
    init_eperms2();
#endif /* CCPERM */
#ifdef CEPERM
#ifdef VERBOSE
    fprintf(stderr, "Initializing edge permutations...\n");
#endif /* VERBOSE */
    init_eperms();
#ifdef VERBOSE
    fprintf(stderr, "Done!\n");
    fprintf(stderr, "Collapsing edge permutations...\n");
#endif /* VERBOSE */
    init_eperms1();
#ifdef VERBOSE
    fprintf(stderr, "Done!\n");
    fprintf(stderr, "Initializing corner permutations...\n");
#endif /* VERBOSE */
    init_cperms();
#ifdef VERBOSE
    fprintf(stderr, "Done!\n");
    fprintf(stderr, "Matching corner permutations...\n");
#endif /* VERBOSE */
    init_cperms2();
#endif /* CEPERM */
#ifdef VERBOSE
    fprintf(stderr, "Done!\n");
    fprintf(stderr, "Initializing data array...\n");
#endif /* VERBOSE */
    for(i = 0; i < MT; i++) {
	turns1[i] = turns2[i] = 0;
    }
#ifdef VERBOSE
    fprintf(stderr, "Done!\n");
#endif /* VERBOSE */
    init_turns(mininit);
    do_pr(0, mininit);
    i = 0;
    while(left > 0) {
	try(i++);
    }
    printf(", %10d maximal\n", gcount);
    if(givemax) {
	printf("Maximal configurations:\n");
	for(i = 0; i < MT; i++) {
	    if(turns2[i] != 0) {
		turns1[i] |= turns2[i];
	    }
	}
	for(i = 0; i < MT; i++) {
	    if((turns1[i] & turns2[i]) != 0) {
		j = turns1[i] & turns2[i];
		turns2[i] &= (j ^ (~(TYPE)0));
		k = 0;
		while(j != 0) {
		    if(j & 1) {
			printf("%8d\n", (i << POW) + k);
		    }
		    k++;
		    j >>= 1;
		}
	    }
	}
    }
    exit(0);
}

