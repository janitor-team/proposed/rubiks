#ifndef IPERM
#define SPERM_MAGIC	8
#else /* IPERM */
ERROR: No sense in IPERM when collapsing on SPERM.
#endif /* IPERM */

static unsigned short sperms_pointer[MAX_SPERMS];
static char sperms_weights[SPERM_MAGIC];
static unsigned short sperms_presents[SPERM_MAGIC];
static int sperms_magic_counter;
#ifdef EQUIV
static char sperms_rotate[MAX_SPERMS][MI];
#endif /* EQUIV */

static void init_sperms1()
{
int i, j, k;
int p[4];
int counted;
#define params	p[0],p[1],p[2],p[3]
#define params1	p+0,p+1,p+2,p+3

    counted = 0;
    for(i = 0; i < MAX_SPERMS; i++) {
	sperms_pointer[i] = 65535;
    }
    for(i = 0; i < MAX_SPERMS; i++) {
	if(sperms_pointer[i] != 65535) {
	    sperms_weights[sperms_pointer[i]]++;
	    continue;
	}
	val_perm4(i,params1);
	sperms_presents[counted] = i;
	sperms_pointer[i] = counted;
	sperms_weights[counted] = 1;
#ifdef EQUIV
	sperms_rotate[i][0] = 1;
	k = 0;
#endif /* EQUIV */
	rotate_ud_sperms(p);
	j = perm4_val(params);
	if(sperms_pointer[j] == 65535) {
	    sperms_pointer[j] = counted;
	}
#ifdef EQUIV
	sperms_rotate[j][++k] = 1;
#endif /* EQUIV */
	rotate_ud_sperms(p);
	j = perm4_val(params);
	if(sperms_pointer[j] == 65535) {
	    sperms_pointer[j] = counted;
	}
#ifdef EQUIV
	sperms_rotate[j][++k] = 1;
#endif /* EQUIV */
	rotate_ud_sperms(p);
	j = perm4_val(params);
	if(sperms_pointer[j] == 65535) {
	    sperms_pointer[j] = counted;
	}
#ifdef EQUIV
	sperms_rotate[j][++k] = 1;
#endif /* EQUIV */
	rotate_ud_sperms(p);
	rotate_rl_sperms(p);
	j = perm4_val(params);
	if(sperms_pointer[j] == 65535) {
	    sperms_pointer[j] = counted;
	}
#ifdef EQUIV
	sperms_rotate[j][++k] = 1;
#endif /* EQUIV */
	rotate_ud_sperms(p);
	j = perm4_val(params);
	if(sperms_pointer[j] == 65535) {
	    sperms_pointer[j] = counted;
	}
#ifdef EQUIV
	sperms_rotate[j][++k] = 1;
#endif /* EQUIV */
	rotate_ud_sperms(p);
	j = perm4_val(params);
	if(sperms_pointer[j] == 65535) {
	    sperms_pointer[j] = counted;
	}
#ifdef EQUIV
	sperms_rotate[j][++k] = 1;
#endif /* EQUIV */
	rotate_ud_sperms(p);
	j = perm4_val(params);
	if(sperms_pointer[j] == 65535) {
	    sperms_pointer[j] = counted;
	}
#ifdef EQUIV
	sperms_rotate[j][++k] = 1;
#endif /* EQUIV */
	rotate_ud_sperms(p);
	rotate_rl_sperms(p);
	flip_sperms(p);
	j = perm4_val(params);
	if(sperms_pointer[j] == 65535) {
	    sperms_pointer[j] = counted;
	}
#ifdef EQUIV
	sperms_rotate[j][++k] = 1;
#endif /* EQUIV */
	rotate_ud_sperms(p);
	j = perm4_val(params);
	if(sperms_pointer[j] == 65535) {
	    sperms_pointer[j] = counted;
	}
#ifdef EQUIV
	sperms_rotate[j][++k] = 1;
#endif /* EQUIV */
	rotate_ud_sperms(p);
	j = perm4_val(params);
	if(sperms_pointer[j] == 65535) {
	    sperms_pointer[j] = counted;
	}
#ifdef EQUIV
	sperms_rotate[j][++k] = 1;
#endif /* EQUIV */
	rotate_ud_sperms(p);
	j = perm4_val(params);
	if(sperms_pointer[j] == 65535) {
	    sperms_pointer[j] = counted;
	}
#ifdef EQUIV
	sperms_rotate[j][++k] = 1;
#endif /* EQUIV */
	rotate_ud_sperms(p);
	rotate_rl_sperms(p);
	j = perm4_val(params);
	if(sperms_pointer[j] == 65535) {
	    sperms_pointer[j] = counted;
	}
#ifdef EQUIV
	sperms_rotate[j][++k] = 1;
#endif /* EQUIV */
	rotate_ud_sperms(p);
	j = perm4_val(params);
	if(sperms_pointer[j] == 65535) {
	    sperms_pointer[j] = counted;
	}
#ifdef EQUIV
	sperms_rotate[j][++k] = 1;
#endif /* EQUIV */
	rotate_ud_sperms(p);
	j = perm4_val(params);
	if(sperms_pointer[j] == 65535) {
	    sperms_pointer[j] = counted;
	}
#ifdef EQUIV
	sperms_rotate[j][++k] = 1;
#endif /* EQUIV */
	rotate_ud_sperms(p);
	j = perm4_val(params);
	if(sperms_pointer[j] == 65535) {
	    sperms_pointer[j] = counted;
	}
#ifdef EQUIV
	sperms_rotate[j][++k] = 1;
#endif /* EQUIV */
	counted++;
    }
#ifdef VERBOSE
    fprintf(stderr, "\tLeft = %d\n", counted);
#endif /* VERBOSE */
    sperms_magic_counter = counted;
}
#undef params
#undef params1

