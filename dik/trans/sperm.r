static void rotate_ud_sperms(p)
int *p;
{
int p1[4], p2[4], i;

    p1[0] = 1;
    p1[1] = 2;
    p1[2] = 3;
    p1[3] = 0;
    for(i = 0; i < 4; i++) {
	p2[i] = p1[p[i]];
    }
    p[0] = p2[3];
    p[1] = p2[0];
    p[2] = p2[1];
    p[3] = p2[2];
}

static void rotate_rl_sperms(p)
int *p;
{
int p1[4], p2[4], i;

    p1[0] = 3;
    p1[1] = 2;
    p1[2] = 1;
    p1[3] = 0;
    for(i = 0; i < 4; i++) {
	p2[i] = p1[p[i]];
    }
    p[0] = p2[3];
    p[1] = p2[2];
    p[2] = p2[1];
    p[3] = p2[0];
}

static void flip_sperms(p)
int *p;
{
int p1[4], p2[4], i;

    p1[0] = 0;
    p1[1] = 1;
    p1[2] = 2;
    p1[3] = 3;
    for(i = 0; i < 4; i++) {
	p2[i] = p1[p[i]];
    }
    p[0] = p2[0];
    p[1] = p2[1];
    p[2] = p2[2];
    p[3] = p2[3];
}

