static unsigned short twists_trans[MAX_TWISTS][MI];

static void init_twists2()
{
int i, j, k;
int t0,t1,t2,t3,t4,t5,t6,t7;
int nt0,nt1,nt2,nt3,nt4,nt5,nt6,nt7;
#define params	nt0,nt1,nt2,nt3,nt4,nt5,nt6,nt7
#define params1	&nt0,&nt1,&nt2,&nt3,&nt4,&nt5,&nt6,&nt7
#define inc(i, j) ((i+j) % 3)

    t0 = t1 = t2 = t3 = t4 = t5 = t6 = t7 = 0;
    for(i = 0; i < MAX_TWISTS; i++) {
	nt0 = t0;
	nt1 = t1;
	nt2 = t2;
	nt3 = t3;
	nt4 = t4;
	nt5 = t5;
	nt6 = t6;
	nt7 = t7;
	twists_trans[i][0] = i;
	k = 0;
	rotate_ud_twists(params1);
	twists_trans[i][inv_trans[++k]] = twist_val(params);
#ifndef ROT2
	rotate_ud_twists(params1);
	twists_trans[i][inv_trans[++k]] = twist_val(params);
	rotate_ud_twists(params1);
	twists_trans[i][inv_trans[++k]] = twist_val(params);
#endif /* ROT2 */
	rotate_ud_twists(params1);
	rotate_rl_twists(params1);
	twists_trans[i][inv_trans[++k]] = twist_val(params);
	rotate_ud_twists(params1);
	twists_trans[i][inv_trans[++k]] = twist_val(params);
#ifndef ROT2
	rotate_ud_twists(params1);
	twists_trans[i][inv_trans[++k]] = twist_val(params);
	rotate_ud_twists(params1);
	twists_trans[i][inv_trans[++k]] = twist_val(params);
#endif /* ROT2 */
	rotate_ud_twists(params1);
	rotate_rl_twists(params1);
	flip_twists(params1);
	twists_trans[i][inv_trans[++k]] = twist_val(params);
	rotate_ud_twists(params1);
	twists_trans[i][inv_trans[++k]] = twist_val(params);
#ifndef ROT2
	rotate_ud_twists(params1);
	twists_trans[i][inv_trans[++k]] = twist_val(params);
	rotate_ud_twists(params1);
	twists_trans[i][inv_trans[++k]] = twist_val(params);
#endif /* ROT2 */
	rotate_ud_twists(params1);
	rotate_rl_twists(params1);
	twists_trans[i][inv_trans[++k]] = twist_val(params);
	rotate_ud_twists(params1);
	twists_trans[i][inv_trans[++k]] = twist_val(params);
#ifndef ROT2
	rotate_ud_twists(params1);
	twists_trans[i][inv_trans[++k]] = twist_val(params);
	rotate_ud_twists(params1);
	twists_trans[i][inv_trans[++k]] = twist_val(params);
#endif /* ROT2 */
	t0 = inc(t0,1);
	if(t0 == 0) {
	    t1 = inc(t1,1);
	    if(t1 == 0) {
		t2 = inc(t2,1);
		if(t2 == 0) {
		    t3 = inc(t3,1);
		    if(t3 == 0) {
			t4 = inc(t4,1);
			if(t4 == 0) {
			    t5 = inc(t5,1);
			    if(t5 == 0) {
				t6 = inc(t6,1);
			    }
			}
		    }
		}
	    }
	}
	j = (t0 + t1 + t2 + t3 + t4 + t5 + t6) % 3;
	t7 = 3 - j;
	if(t7 == 3) {
	    t7 = 0;
	}
    }
}
#undef params
#undef params1
#undef inc

