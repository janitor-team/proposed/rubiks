#ifndef ROT2
ERROR: Collapsing with FLIP used requires ROT2
#else /* ROT2 */
#define FLIP_MAGIC	336
#endif /* ROT2 */

static unsigned short flips_pointer[MAX_FLIPS];
static char flips_weights[FLIP_MAGIC];
static unsigned short flips_presents[FLIP_MAGIC];
static int flips_magic_counter;
#ifdef EQUIV
static char flips_rotate[MAX_FLIPS][MI];
#endif /* EQUIV */

static void init_flips1()
{
int i, j, k;
int f0, f1, f2, f3, f4, f5, f6, f7, f8, f9, fa, fb;
int nf0, nf1, nf2, nf3, nf4, nf5, nf6, nf7, nf8, nf9, nfa, nfb;
int counted, val;
#define inv(i)	((i) ^ 1)
#define params	nf0,nf1,nf2,nf3,nf4,nf5,nf6,nf7,nf8,nf9,nfa,nfb
#define params1	&nf0,&nf1,&nf2,&nf3,&nf4,&nf5,&nf6,&nf7,&nf8,&nf9,&nfa,&nfb

    counted = 0;
    for(i = 0; i < MAX_FLIPS; i++) {
	flips_pointer[i] = 65535;
    }
    f0 = f1 = f2 = f3 = f4 = f5 = f6 = f7 = f8 = f9 = fa = fb = 0;
    for(i = 0; i < MAX_FLIPS; i++) {
	if(flips_pointer[i] != 65535) {
	    flips_weights[flips_pointer[i]]++;
	    goto fin;
	}
	flips_presents[counted] = i;
	flips_pointer[i] = counted;
	flips_weights[counted] = 1;
#ifdef EQUIV
	flips_rotate[i][0] = 1;
	k = 0;
#endif /* EQUIV */
	nf0 = f0;
	nf1 = f1;
	nf2 = f2;
	nf3 = f3;
	nf4 = f4;
	nf5 = f5;
	nf6 = f6;
	nf7 = f7;
	nf8 = f8;
	nf9 = f9;
	nfa = fa;
	nfb = fb;
	rotate_ud_flips(params1);
	j = flip_val(params);
	if(flips_pointer[j] == 65535) {
	    flips_pointer[j] = counted;
	}
#ifdef EQUIV
	flips_rotate[j][++k] = 1;
#endif /* EQUIV */
#ifndef ROT2
	rotate_ud_flips(params1);
	j = flip_val(params);
	if(flips_pointer[j] == 65535) {
	    flips_pointer[j] = counted;
	}
#ifdef EQUIV
	flips_rotate[j][++k] = 1;
#endif /* EQUIV */
	rotate_ud_flips(params1);
	j = flip_val(params);
	if(flips_pointer[j] == 65535) {
	    flips_pointer[j] = counted;
	}
#ifdef EQUIV
	flips_rotate[j][++k] = 1;
#endif /* EQUIV */
#endif /* ROT2 */
	rotate_ud_flips(params1);
	rotate_rl_flips(params1);
	j = flip_val(params);
	if(flips_pointer[j] == 65535) {
	    flips_pointer[j] = counted;
	}
#ifdef EQUIV
	flips_rotate[j][++k] = 1;
#endif /* EQUIV */
	rotate_ud_flips(params1);
	j = flip_val(params);
	if(flips_pointer[j] == 65535) {
	    flips_pointer[j] = counted;
	}
#ifdef EQUIV
	flips_rotate[j][++k] = 1;
#endif /* EQUIV */
#ifndef ROT2
	rotate_ud_flips(params1);
	j = flip_val(params);
	if(flips_pointer[j] == 65535) {
	    flips_pointer[j] = counted;
	}
#ifdef EQUIV
	flips_rotate[j][++k] = 1;
#endif /* EQUIV */
	rotate_ud_flips(params1);
	j = flip_val(params);
	if(flips_pointer[j] == 65535) {
	    flips_pointer[j] = counted;
	}
#ifdef EQUIV
	flips_rotate[j][++k] = 1;
#endif /* EQUIV */
#endif /* ROT2 */
	rotate_ud_flips(params1);
	rotate_rl_flips(params1);
	flip_flips(params1);
	j = flip_val(params);
	if(flips_pointer[j] == 65535) {
	    flips_pointer[j] = counted;
	}
#ifdef EQUIV
	flips_rotate[j][++k] = 1;
#endif /* EQUIV */
	rotate_ud_flips(params1);
	j = flip_val(params);
	if(flips_pointer[j] == 65535) {
	    flips_pointer[j] = counted;
	}
#ifdef EQUIV
	flips_rotate[j][++k] = 1;
#endif /* EQUIV */
#ifndef ROT2
	rotate_ud_flips(params1);
	j = flip_val(params);
	if(flips_pointer[j] == 65535) {
	    flips_pointer[j] = counted;
	}
#ifdef EQUIV
	flips_rotate[j][++k] = 1;
#endif /* EQUIV */
	rotate_ud_flips(params1);
	j = flip_val(params);
	if(flips_pointer[j] == 65535) {
	    flips_pointer[j] = counted;
	}
#ifdef EQUIV
	flips_rotate[j][++k] = 1;
#endif /* EQUIV */
#endif /* ROT2 */
	rotate_ud_flips(params1);
	rotate_rl_flips(params1);
	j = flip_val(params);
	if(flips_pointer[j] == 65535) {
	    flips_pointer[j] = counted;
	}
#ifdef EQUIV
	flips_rotate[j][++k] = 1;
#endif /* EQUIV */
	rotate_ud_flips(params1);
	j = flip_val(params);
	if(flips_pointer[j] == 65535) {
	    flips_pointer[j] = counted;
	}
#ifdef EQUIV
	flips_rotate[j][++k] = 1;
#endif /* EQUIV */
#ifndef ROT2
	rotate_ud_flips(params1);
	j = flip_val(params);
	if(flips_pointer[j] == 65535) {
	    flips_pointer[j] = counted;
	}
#ifdef EQUIV
	flips_rotate[j][++k] = 1;
#endif /* EQUIV */
	rotate_ud_flips(params1);
	j = flip_val(params);
	if(flips_pointer[j] == 65535) {
	    flips_pointer[j] = counted;
	}
#ifdef EQUIV
	flips_rotate[j][++k] = 1;
#endif /* EQUIV */
#endif /* ROT2 */
	counted++;
fin:
	f0 = inv(f0);
	if(f0 == 0) {
	    f1 = inv(f1);
	    if(f1 == 0) {
		f2 = inv(f2);
		if(f2 == 0) {
		    f3 = inv(f3);
		    if(f3 == 0) {
			f4 = inv(f4);
			if(f4 == 0) {
			    f5 = inv(f5);
			    if(f5 == 0) {
				f6 = inv(f6);
				if(f6 == 0) {
				    f7 = inv(f7);
				    if(f7 == 0) {
					f8 = inv(f8);
					if(f8 == 0) {
					    f9 = inv(f9);
					    if(f9 == 0) {
						fa = inv(fa);
					    }
					}
				    }
				}
			    }
			}
		    }
		}
	    }
	}
	j = (f0 + f1 + f2 + f3 + f4 + f5 + f6 + f7 + f8 + f9 + fa) & 1;
	fb = j;
    }
#ifdef VERBOSE
    fprintf(stderr, "\tLeft = %d\n", counted);
#endif /* VERBOSE */
    flips_magic_counter = counted;
}
#undef inv
#undef params
#undef params1

