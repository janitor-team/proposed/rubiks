#define inv(i)	((i)^1)

#ifndef ROT2
ERROR: Collapsing with FLIP used requires ROT2
#endif /* ROT2 */

void rotate_ud_flips(f0,f1,f2,f3,f4,f5,f6,f7,f8,f9,fa,fb)
int *f0,*f1,*f2,*f3,*f4,*f5,*f6,*f7,*f8,*f9,*fa,*fb;
{
int tmp;

    tmp = *f0;
    *f0 = *f2;
    *f2 = tmp;
    tmp = *f1;
    *f1 = *f3;
    *f3 = tmp;
    tmp = *f4;
    *f4 = *f6;
    *f6 = tmp;
    tmp = *f5;
    *f5 = *f7;
    *f7 = tmp;
    tmp = *f8;
    *f8 = *fa;
    *fa = tmp;
    tmp = *f9;
    *f9 = *fb;
    *fb = tmp;
}

void rotate_rl_flips(f0,f1,f2,f3,f4,f5,f6,f7,f8,f9,fa,fb)
int *f0,*f1,*f2,*f3,*f4,*f5,*f6,*f7,*f8,*f9,*fa,*fb;
{
int tmp;

    tmp = *f0;
    *f0 = *f6;
    *f6 = tmp;
    tmp = *f1;
    *f1 = *f5;
    *f5 = tmp;
    tmp = *f2;
    *f2 = *f4;
    *f4 = tmp;
    tmp = *f3;
    *f3 = *f7;
    *f7 = tmp;
    tmp = *f8;
    *f8 = *fb;
    *fb = tmp;
    tmp = *f9;
    *f9 = *fa;
    *fa = tmp;
}

void flip_flips(f0,f1,f2,f3,f4,f5,f6,f7,f8,f9,fa,fb)
int *f0,*f1,*f2,*f3,*f4,*f5,*f6,*f7,*f8,*f9,*fa,*fb;
{
int tmp;

    tmp = *f0;
    *f0 = *f4;
    *f4 = tmp;
    tmp = *f1;
    *f1 = *f5;
    *f5 = tmp;
    tmp = *f2;
    *f2 = *f6;
    *f6 = tmp;
    tmp = *f3;
    *f3 = *f7;
    *f7 = tmp;
}
#undef inv

