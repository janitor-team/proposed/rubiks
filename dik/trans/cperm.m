static unsigned short cperms_trans[MAX_CPERMS][MI];

static void init_cperms2()
{
int i, k;
int p[8];
#define params	p[0],p[1],p[2],p[3],p[4],p[5],p[6],p[7]
#define params1	p+0,p+1,p+2,p+3,p+4,p+5,p+6,p+7

    for(i = 0; i < MAX_CPERMS; i++) {
	val_perm8(i,params1);
	cperms_trans[i][0] = i;
	k = 0;
	rotate_ud_cperms(p);
	cperms_trans[i][inv_trans[++k]] = perm8_val(params);
	rotate_ud_cperms(p);
	cperms_trans[i][inv_trans[++k]] = perm8_val(params);
	rotate_ud_cperms(p);
	cperms_trans[i][inv_trans[++k]] = perm8_val(params);
	rotate_ud_cperms(p);
	rotate_rl_cperms(p);
	cperms_trans[i][inv_trans[++k]] = perm8_val(params);
	rotate_ud_cperms(p);
	cperms_trans[i][inv_trans[++k]] = perm8_val(params);
	rotate_ud_cperms(p);
	cperms_trans[i][inv_trans[++k]] = perm8_val(params);
	rotate_ud_cperms(p);
	cperms_trans[i][inv_trans[++k]] = perm8_val(params);
	rotate_ud_cperms(p);
	rotate_rl_cperms(p);
	flip_cperms(p);
	cperms_trans[i][inv_trans[++k]] = perm8_val(params);
	rotate_ud_cperms(p);
	cperms_trans[i][inv_trans[++k]] = perm8_val(params);
	rotate_ud_cperms(p);
	cperms_trans[i][inv_trans[++k]] = perm8_val(params);
	rotate_ud_cperms(p);
	cperms_trans[i][inv_trans[++k]] = perm8_val(params);
	rotate_ud_cperms(p);
	rotate_rl_cperms(p);
	cperms_trans[i][inv_trans[++k]] = perm8_val(params);
	rotate_ud_cperms(p);
	cperms_trans[i][inv_trans[++k]] = perm8_val(params);
	rotate_ud_cperms(p);
	cperms_trans[i][inv_trans[++k]] = perm8_val(params);
	rotate_ud_cperms(p);
	cperms_trans[i][inv_trans[++k]] = perm8_val(params);
#ifdef IPERM
	rotate_ud_cperms(p);
	rotate_rl_cperms(p);
	flip_cperms(p);
	inv_perm8(p);
	cperms_trans[i][inv_trans[++k]] = perm8_val(params);
	rotate_ud_cperms(p);
	cperms_trans[i][inv_trans[++k]] = perm8_val(params);
	rotate_ud_cperms(p);
	cperms_trans[i][inv_trans[++k]] = perm8_val(params);
	rotate_ud_cperms(p);
	cperms_trans[i][inv_trans[++k]] = perm8_val(params);
	rotate_ud_cperms(p);
	rotate_rl_cperms(p);
	cperms_trans[i][inv_trans[++k]] = perm8_val(params);
	rotate_ud_cperms(p);
	cperms_trans[i][inv_trans[++k]] = perm8_val(params);
	rotate_ud_cperms(p);
	cperms_trans[i][inv_trans[++k]] = perm8_val(params);
	rotate_ud_cperms(p);
	cperms_trans[i][inv_trans[++k]] = perm8_val(params);
	rotate_ud_cperms(p);
	rotate_rl_cperms(p);
	flip_cperms(p);
	cperms_trans[i][inv_trans[++k]] = perm8_val(params);
	rotate_ud_cperms(p);
	cperms_trans[i][inv_trans[++k]] = perm8_val(params);
	rotate_ud_cperms(p);
	cperms_trans[i][inv_trans[++k]] = perm8_val(params);
	rotate_ud_cperms(p);
	cperms_trans[i][inv_trans[++k]] = perm8_val(params);
	rotate_ud_cperms(p);
	rotate_rl_cperms(p);
	cperms_trans[i][inv_trans[++k]] = perm8_val(params);
	rotate_ud_cperms(p);
	cperms_trans[i][inv_trans[++k]] = perm8_val(params);
	rotate_ud_cperms(p);
	cperms_trans[i][inv_trans[++k]] = perm8_val(params);
	rotate_ud_cperms(p);
	cperms_trans[i][inv_trans[++k]] = perm8_val(params);
#endif /* IPERM */
    }
}
#undef params
#undef params1

