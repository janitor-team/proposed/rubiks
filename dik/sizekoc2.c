#include <stdio.h>

#define EQUIV
#define NOMAX
#define SUBGROUP
#include "trans/inv.a"
#include "trans/perm4.a"
#include "trans/perm8.a"
#include "trans/cperm.a"
#include "trans/cperm.r"
#include "trans/cperm.c"
#include "trans/eperm.a"
#include "trans/eperm.r"
#include "trans/eperm.m"
#include "trans/sperm.a"
#include "trans/sperm.r"
#include "trans/sperm.m"

#define MAX_TURNS    (CPERM_MAGIC * MAX_EPERMS * MAX_SPERMS)

#include "longtype.h"

TYPE turns1[MT];
TYPE turns2[MT];
long left = MAX_TURNS;
int quarter;
int maximal;
int gcount;
int givemax;

do_pr(turns, count)
int turns, count;
{
    if(maximal != 0 && turns != 1) {
	printf(", %10d maximal", maximal);
    }
    printf("\n");
    gcount = count;
    printf("%10d with %2d turns", count, turns);
    fflush(stdout);
}

try_one(num, count)
int num, count;
{
int cperm, ncperm, ncperm1, cperm0;
int eperm, neperm, neperm1;
int sperm, nsperm, nsperm1;
int i, j, nnum, counted = 0, k, l, max, wcounted = 0, m;

    m = num / MAX_EPERMS;
    sperm = num  - m * MAX_EPERMS;
    cperm0 = m / MAX_SPERMS;
    eperm = m - cperm0 * MAX_SPERMS;
    max = cperms_weights[cperm0];
    cperm = cperms_presents[cperm0];
    for(i = 0; i < 6; i++) {
	ncperm = cperm;
	neperm = eperm;
	nsperm = sperm;
	for(j = 0; j < 3; j++) {
	    ncperm = cperms[ncperm][i];
	    neperm = eperms[neperm][i];
	    nsperm = sperms[nsperm][i];
	    if(quarter && j == 1) {
		continue;
	    }
	    ncperm1 = cperms_pointer[ncperm];
	    for(m = 0; m < MI; m++) {
		if(cperms_rotate[ncperm][m]) {
		    neperm1 = eperms_trans[neperm][m];
		    nsperm1 = sperms_trans[nsperm][m];
		    nnum = ncperm1 * MAX_SPERMS + neperm1;
		    nnum = nnum * MAX_EPERMS + nsperm1;
		    k = (nnum >> POW);
		    l = (1 << (nnum & MASK));
		    if((turns1[k] & l) == 0) {
			if((turns2[k] & l) == 0) {
			    turns2[k] |= l;
			    counted++;
			    wcounted += cperms_weights[ncperm1];
			}
			max = 0;
		    }
		}
	    }
	}
    }
#ifdef IPERM
    cperm = cperms_inv[cperm];
    eperm = eperms_inv[eperm];
    sperm = sperms_inv[sperm];
    for(i = 0; i < 6; i++) {
	ncperm = cperm;
	neperm = eperm;
	nsperm = sperm;
	for(j = 0; j < 3; j++) {
	    ncperm = cperms[ncperm][i];
	    neperm = eperms[neperm][i];
	    nsperm = sperms[nsperm][i];
	    if(quarter && j == 1) {
		continue;
	    }
	    ncperm1 = cperms_pointer[ncperm];
	    for(m = 0; m < MI; m++) {
		if(cperms_rotate[ncperm][m]) {
		    neperm1 = eperms_trans[neperm][m];
		    nsperm1 = sperms_trans[nsperm][m];
		    nnum = ncperm1 * MAX_SPERMS + neperm1;
		    nnum = nnum * MAX_EPERMS + nsperm1;
		    k = (nnum >> POW);
		    l = (1 << (nnum & MASK));
		    if((turns1[k] & l) == 0) {
			if((turns2[k] & l) == 0) {
			    turns2[k] |= l;
			    counted++;
			    wcounted += cperms_weights[ncperm1];
			}
			max = 0;
		    }
		}
	    }
	}
    }
#endif /* IPERM */
    left -= counted;
    maximal += max;
    return wcounted;
}

try(count)
int count;
{
int i, counted = 0, k;
TYPE j;

    maximal = 0;
    for(i = 0; i < MT; i++) {
	if(turns2[i] != 0) {
	    turns1[i] |= turns2[i];
	}
    }
    for(i = 0; i < MT; i++) {
	if((turns1[i] & turns2[i]) != 0) {
	    j = turns1[i] & turns2[i];
	    turns2[i] &= (j ^ (~(TYPE)0));
	    k = 0;
	    while(j != 0) {
		if(j & 1) {
		    counted += try_one((i << POW) + k, count + 1);
		}
		k++;
		j >>= 1;
	    }
	}
    }
    do_pr(count + 1, counted);
}

main(argc, argv)
int argc;
char *argv[];
{
int i, k, error = 0;
TYPE j;

    quarter = -1;
    givemax = -1;
    if(argc > 3) {
	fprintf(stderr, "Too many parameters\n");
	exit(1);
    }
    while(argc > 1) {
	if(!strcmp(argv[1], "-q")) {
	    if(quarter >= 0) {
		error = 1;
	    }
	    quarter = 1;
	} else if(!strcmp(argv[1], "-m")) {
	    if(givemax >= 0) {
		error = 1;
	    }
	    givemax = 1;
	}
	argc--;
	argv++;
    }
    if(error) {
	fprintf(stderr, "usage: sizekoc2 [-q] [-m]\n");
	exit(1);
    }
    if(quarter < 0) {
	quarter = 0;
    }
    if(givemax < 0) {
	givemax = 0;
    }
#ifdef VERBOSE
    fprintf(stderr, "Initializing corner permutations...\n");
#endif /* VERBOSE */
    init_cperms();
#ifdef VERBOSE
    fprintf(stderr, "Done!\n");
    fprintf(stderr, "Collapsing corner permutations...\n");
#endif /* VERBOSE */
    init_cperms1();
#ifdef VERBOSE
    fprintf(stderr, "Done!\n");
    fprintf(stderr, "Initializing edge permutations...\n");
#endif /* VERBOSE */
    init_eperms();
#ifdef VERBOSE
    fprintf(stderr, "Done!\n");
    fprintf(stderr, "Matching edge permutations...\n");
#endif /* VERBOSE */
    init_eperms2();
#ifdef VERBOSE
    fprintf(stderr, "Done!\n");
    fprintf(stderr, "Initializing slice permutations...\n");
#endif /* VERBOSE */
    init_sperms();
#ifdef VERBOSE
    fprintf(stderr, "Done!\n");
    fprintf(stderr, "Matching slice permutations...\n");
#endif /* VERBOSE */
    init_sperms2();
#ifdef VERBOSE
    fprintf(stderr, "Done!\n");
    fprintf(stderr, "Initializing data array...\n");
#endif /* VERBOSE */
    for(i = 0; i < MT; i++) {
	turns1[i] = turns2[i] = 0;
    }
#ifdef VERBOSE
    fprintf(stderr, "Done!\n");
#endif /* VERBOSE */
    turns2[0] = 1;
    left--;
    do_pr(0, 1);
    i = 0;
    while(left > 0) {
	try(i++);
    }
    printf(", %10d maximal\n", gcount);
    if(givemax) {
	printf("Maximal configurations:\n");
	for(i = 0; i < MT; i++) {
	    if(turns2[i] != 0) {
		turns1[i] |= turns2[i];
	    }
	}
	for(i = 0; i < MT; i++) {
	    if((turns1[i] & turns2[i]) != 0) {
		j = turns1[i] & turns2[i];
		turns2[i] &= (j ^ (~(TYPE)0));
		k = 0;
		while(j != 0) {
		    if(j & 1) {
			printf("%8d\n", (i << POW) + k);
		    }
		    k++;
		    j >>= 1;
		}
	    }
	}
    }
    exit(0);
}

