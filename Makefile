DIRS=dietz/cu2 dietz/mcube dietz/solver dik reid
#INSTALL=`which install` 
PREFIX=/usr
BINDIR=$(PREFIX)/bin

all clean:
	for dir in $(DIRS); do \
		(cd $${dir} && make $@)\
	done

distclean: clean
	for dir in $(DIRS); do \
		(cd $${dir} && make distclean)\
	done

install: all
	mkdir -p $(DESTDIR)$(BINDIR)
	$(INSTALL) reid/optimal $(DESTDIR)$(BINDIR)
	$(INSTALL) dietz/solver/cubex $(DESTDIR)$(BINDIR)
	$(INSTALL) dietz/mcube/mcube $(DESTDIR)$(BINDIR)
	$(INSTALL) dietz/cu2/cu2 $(DESTDIR)$(BINDIR)
	$(INSTALL) dik/dikcube $(DESTDIR)$(BINDIR)
	$(INSTALL) dik/size222 $(DESTDIR)$(BINDIR)
